package com.company;

public class Main {

    public static void main(String[] args) {

        SimpleCalculator simpleCalculator=new SimpleCalculator();
        simpleCalculator.setFirstNumber(5.0);
        simpleCalculator.setSecondNumber(4);
        System.out.println("Adding "+simpleCalculator.getFirstNumber()+" and "+simpleCalculator.getSecondNumber()+" is "+simpleCalculator.getAdditionResult());
        System.out.println("Subtract "+simpleCalculator.getFirstNumber()+" and "+simpleCalculator.getSecondNumber()+" is "+simpleCalculator.getSubtractionResult());
        System.out.println("Multiply "+simpleCalculator.getFirstNumber()+" and "+simpleCalculator.getSecondNumber()+" is "+simpleCalculator.getMultiplicationResult());
        System.out.println("Divide "+simpleCalculator.getFirstNumber()+" and "+simpleCalculator.getSecondNumber()+" is "+simpleCalculator.getDivisionResult());
        simpleCalculator.setFirstNumber(5.25);
        simpleCalculator.setSecondNumber(0);
        System.out.println("Adding "+simpleCalculator.getFirstNumber()+" and "+simpleCalculator.getSecondNumber()+" is "+simpleCalculator.getAdditionResult());
        System.out.println("Subtract "+simpleCalculator.getFirstNumber()+" and "+simpleCalculator.getSecondNumber()+" is "+simpleCalculator.getSubtractionResult());

    }
}
