package com.company;

public class Main {

    public static void main(String[] args) {

        Wall wall = new Wall(5,4);
        System.out.println("Area= "+wall.getArea());
        wall.setHeight(-1.5);
        System.out.println("Width: "+wall.getWidth());
        System.out.println("Height= "+wall.getHeight());
        System.out.println("Area= "+wall.getArea());

        System.out.println("Next");
        Wall wall2=new Wall(1.125, -1.0);
        System.out.println("Area= "+wall2.getArea());
        System.out.println("Width: "+wall2.getWidth());
        System.out.println("Height= "+wall2.getHeight());
        System.out.println("Area= "+wall2.getArea());
    }
}
