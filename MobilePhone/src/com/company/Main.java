package com.company;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);
    private static MobilePhone mobilePhone = new MobilePhone("0039 330 4404");
    private static void printInstructions() {
        System.out.println("\nAvailable actions:\npress");
        System.out.println("0  - Instructions\n" +
                "1  - to print contacts\n" +
                "2  - to add a new contact\n" +
                "3  - to update existing by the new contact\n" +
                "4  - to remove an existing contact\n" +
                "5  - query if an existing contact exists\n" +
                "6  - quit");
        System.out.println("Choose your action: ");
    }

    private static void createContact()
    {
        System.out.println("Enter contact name");
        String name = scanner.nextLine();
        System.out.println("Enter number");
        String number = scanner.nextLine();
        if(mobilePhone.queryContact(name)==null)
        {
            Contact newContact=Contact.createContact(name,number);
            mobilePhone.addNewContact(newContact);
            System.out.println("Added successfully");
        }
        else
        {
            System.out.println("Such contact name already exists");
        }

    }
    private static void updateContact()
    {
        System.out.println("Give contact name to edit");
        String name=scanner.nextLine();
        System.out.println("Give new contact name");
        String nameNew=scanner.nextLine();
        System.out.println("Give new contact number");
        String numberNew=scanner.nextLine();

        if(mobilePhone.queryContact(name)==null)
        {
            System.out.println("Such contact does not exist");
        }
        else
        {
            Contact newContact=Contact.createContact(nameNew,numberNew);
            mobilePhone.updateContact(mobilePhone.queryContact(name),newContact);
        }


    }

    private static void removeContact()
    {
        System.out.println("Give contact name to remove");
        String name=scanner.nextLine();


        if(mobilePhone.queryContact(name)==null)
        {
            System.out.println("Such contact does not exist");
        }
        else
        {
            mobilePhone.removeContact(mobilePhone.queryContact(name));
            System.out.println("Deleted successfully");
        }
    }
    private static void searchForItem()
    {
        System.out.println("Give contact name to find");
        String name=scanner.nextLine();
        if(mobilePhone.queryContact(name)==null)
        {
            System.out.println("Such contact does not exist");
        }
        else
        {
            System.out.println("Contact: ");
            System.out.println("Name: "+mobilePhone.queryContact(name).getName()+" and number is "+mobilePhone.queryContact(name).getPhoneNumber());

        }
    }
    public static void main(String[] args) {
        boolean quit=false;
        int choice=0;
        printInstructions();
        while(!quit)
        {
            System.out.println("Enter your choice ");
            choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice)
            {
                case 0:
                    printInstructions();
                    break;
                case 1:
                    mobilePhone.printContacts();
                    break;
                case 2:
                    createContact();
                    break;
                case 3:
                    updateContact();
                    break;
                case 4:
                    removeContact();
                    break;
                case 5:
                    searchForItem();
                    break;
                case 6:
                    quit=true;
                    break;
            }
        }
    }
}
