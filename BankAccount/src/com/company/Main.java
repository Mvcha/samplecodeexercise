package com.company;

public class Main {

    public static void main(String[] args) {

        Account firstAccount = new Account();
        System.out.println(firstAccount.getCustomerName());


        Account secondAccount = new Account("Michael", "test@test.pl", "12345");
        System.out.println(secondAccount.getAccountNumber() + " customer name is " + secondAccount.getCustomerName());
        secondAccount.getAccountNumber();

        System.out.println("Next user");
        Account johnAccount = new Account("11111", 111, "John", "john@email.com", "555666222");
        System.out.println("Customer name is " + johnAccount.getCustomerName() + " and number account is " + johnAccount.getAccountNumber());
        johnAccount.deposit(100);
        System.out.println("Balance is: " + johnAccount.getBalance());

        System.out.println("VIP PERSON");
        VipPerson person1 = new VipPerson();
        System.out.println(person1.getName());

        VipPerson person2 = new VipPerson("Bob", 2500.00);
        System.out.println(person2.getName());

        VipPerson person3 = new VipPerson("Tim", 100.00, "tim@email.com");
        System.out.println(person3.getName());
        System.out.println(person3.getEmailAddress());

    }
}
