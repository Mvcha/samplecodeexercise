public class FirstLastDigitSum {

    public static int sumFirstAndLastDigit(int number)
    {
        int originalNumber=number;
        if(number<0)
        {
            return -1;
        }
        int count =0;
        while(number!=0)
        {
            number=Math.round(number/10);
            count++;
        }
        count--;

        if (count==0)
        {
            return originalNumber+originalNumber;
        }
        int lastDigit=originalNumber%10;
        int firstDigit=(int) (originalNumber/Math.pow(10, count));
        return lastDigit+firstDigit;

    }
}
