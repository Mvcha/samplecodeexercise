package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5};
        reverse(array);
    }

    private static void reverse(int[] array)
    {

        int j=array.length-1;
        int tempValue;
        System.out.println("Array = " + Arrays.toString(array));
        for(int i=0; i<array.length-1;i++)
        {
            if(i!=j)
            {
                tempValue=array[i];
                array[i]=array[j];
                array[j]=tempValue;
                j--;
            }
        }
        System.out.println("Reversed array = " + Arrays.toString(array));

    }
}
