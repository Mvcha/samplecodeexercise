package com.company;

public class Sofa {
    private int numberOfSeats;
    private String color;

    public void cover()
    {
        System.out.println("Covering sofa");
    }

    public Sofa(int numberOfSeats, String color) {
        this.numberOfSeats = numberOfSeats;
        this.color = color;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public String getColor() {
        return color;
    }
}
