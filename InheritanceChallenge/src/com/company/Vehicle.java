package com.company;

public class Vehicle {

    private String name;
    private String size;

    private int currentVelocity;
    private int currentDirection;
    private boolean engineIsRunning=false;



    public Vehicle(String name, String size) {
        this.name = name;
        this.size = size;

        this.currentVelocity=0;
        this.currentDirection=0;
    }

    public void steer(int direction)
    {
        this.currentDirection+=direction;
        System.out.println("Vehicle.steer(): steering at "+currentDirection+ "degress.");
    }

    public void stop(){
        this.currentVelocity=0;
    }

    public void powerOnEngine()
    {
        if(engineIsRunning==false)
        {
            System.out.println("I'm starting the vehicle");
            engineIsRunning=true;
        }
        else
        {
            System.out.println("I can't start the vehicle. It was start earlier.");
        }
    }

    public void powerOffEngine()
    {
        if(engineIsRunning==true)
        {
            System.out.println("I stop engine successfully");
        }
        else
        {
            System.out.println("I can't stop the engine. It wasn't running.");
        }
    }

    public void isRunning()
    {
        if(engineIsRunning==false)
        {
            System.out.println("Vehicle isn't running");

        }
        else
        {
            System.out.println("Vehicle is running.");
        }

    }
    public void move(int velocity, int direction)
    {
        this.currentVelocity=velocity;
        this.currentDirection=direction;
        System.out.println("Vehicle.move(): Moving at "+currentVelocity+" in direction "+currentDirection);
    }

    public String getName() {
        return name;
    }


    public int getCurrentVelocity() {
        return currentVelocity;
    }

    public int getCurrentDirection() {
        return currentDirection;
    }

    public boolean isEngineIsRunning() {
        return engineIsRunning;
    }
}
