public class BarkingDog {

    public static boolean shouldWakeUp(boolean barking, int hourOfDay)
    {
        if(hourOfDay<8 && hourOfDay>=0 && barking==true || hourOfDay>22 && hourOfDay<24 && barking==true )
        {
            System.out.println(true);
            return true;
        }
        else
        {
            System.out.println(false);
            return false;
        }
    }
}
