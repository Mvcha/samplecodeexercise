public class Statement {

    public static void switchValueChallenge(char switchValue)
    {

        switch(switchValue)
        {
            case 'A':
                System.out.println("Somebody type A");
                break;
            case 'B':
                System.out.println("Somebody type B");
                break;
            case 'C':
                System.out.println("Somebody type C");
                break;
            default:
                System.out.println("Somebody type something different than ABC");
        }
    }

}
