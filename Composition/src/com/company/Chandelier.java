package com.company;

public class Chandelier {
    private int numberOfBulbs;
    private String manufacturer;

    public void turnOn()
    {
        System.out.println("Chandelier -> turn on");
    }
    public Chandelier(int numberOfBulbs, String manufacturer) {
        this.numberOfBulbs = numberOfBulbs;
        this.manufacturer = manufacturer;
    }

    public int getNumberOfBulbs() {
        return numberOfBulbs;
    }

    public String getManufacturer() {
        return manufacturer;
    }
}
