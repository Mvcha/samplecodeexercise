public class GreatestCommonDivisor {

    public static int getGreatestCommonDivisor(int firstNumber, int secondNumber)
    {
        if(firstNumber<10 || secondNumber<10)
        {
            return -1;
        }

        if(firstNumber>=secondNumber)
        {
            if(secondNumber%firstNumber==0)
            {
                return firstNumber;
            }
            else
            {
                for(int i=firstNumber; i>0; i--)
                {
                    if(firstNumber%i==0)
                    {
                        if(secondNumber%i==0)
                        {
                            return i;
                        }
                    }

                }
                return -1;
            }
        }
        else
        {
                for(int i=secondNumber; i>0; i--)
                {
                    if(secondNumber%i==0)
                    {
                        if(firstNumber%i==0)
                        {
                            return i;
                        }
                    }

                }
                return -1;
        }

    }
}
