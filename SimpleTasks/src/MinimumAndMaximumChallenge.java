import java.util.Scanner;

public class MinimumAndMaximumChallenge {

    public static void minAndMaximumInput()
    {
        Scanner scanner=new Scanner(System.in);
        int min=0;
        int max=0;


        while(true)
        {
            System.out.println("Enter number");
            boolean isInt= scanner.hasNextInt();
            if(isInt)
            {
                int number=scanner.nextInt();
                if(number<min)
                {
                    min=number;
                }
                else if(number>max)
                {
                    max=number;
                }
            }
            else
            {
                break;
            }
        }
        scanner.close();
        System.out.println("Minimum is "+min+" and maximum is "+max);
    }
}
