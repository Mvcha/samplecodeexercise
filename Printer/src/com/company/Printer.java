package com.company;

public class Printer {

    private int tonerLevel;
    private int pagesPrinted;
    private boolean duplex;


    public int addToner(int tonerAmount){
        if (tonerAmount>0 && tonerAmount<100)
        {
            tonerLevel+=tonerAmount;
            int restToner=0;
            if(tonerLevel+tonerAmount>100)
            {
                restToner= tonerLevel-100;
                tonerLevel=100;
            }
            //System.out.println("Toner is filling....");
           // System.out.println("Toner level is "+tonerLevel+". Rest toner to fill up is "+restToner);
            return tonerLevel;
        }
        else
            return -1;

    }

    public int printPages(int pages){
        int pagesToPrint;

        if(duplex==true)
        {

           // System.out.println("Duplex mode printing is enable");
            pagesToPrint= (int) Math.ceil(pages/2.0);

           // System.out.println("Printed pages "+pagesToPrint+ " and used "+pagesToPrint + " papers");
            pagesPrinted+=pagesToPrint;
            return pagesToPrint;
        }
        else
        {
            pagesToPrint=pages;
            pagesPrinted+=pages;
            return pagesToPrint;
        }
    }


    public int getPagesPrinted() {
        return pagesPrinted;
    }

    public Printer(int tonerLevel, boolean duplex) {
        if(tonerLevel>0 || tonerLevel<=100)
        {
            this.tonerLevel = tonerLevel;
        }
        else
            this.tonerLevel=-1;

        this.pagesPrinted = 0;
        this.duplex = duplex;
    }
}
