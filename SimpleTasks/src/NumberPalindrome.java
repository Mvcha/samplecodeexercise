public class NumberPalindrome {

    public static boolean isPalindrome(int number) {
        int reverseNumber=0;
        int count=0;
        int originalNumber = number;

        while(number!=0)
        {
            number=Math.round(number/10);
            count++;
        }
        count--;
        number=originalNumber;

        for(int i=0;count>=0; count--)
        {
            int lastDigit = number %10;
            number=Math.round(number/10);
            if(count!=0)
            {
                reverseNumber+=lastDigit*Math.pow(10,count);

            }
            else
            {
                reverseNumber+=lastDigit;
            }
        }

        if(originalNumber==reverseNumber)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
