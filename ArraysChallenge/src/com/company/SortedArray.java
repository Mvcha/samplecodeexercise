package com.company;

import java.util.Scanner;

public class SortedArray {


    public static int[] getIntegers(int size)
    {
        System.out.println("Give me number");
        int[] intArray=new int[size];
        Scanner scanner=new Scanner(System.in);
        for(int i=0; i<size; i++)
        {
            intArray[i]=scanner.nextInt();
        }

        return intArray;
    }
    public static void printArray(int[] intArray)
    {
        for (int i=0; i<intArray.length;i++)
        {
            System.out.println("Element "+i+" contents "+intArray[i]);
        }
    }

    public static int[] sortIntegers(int[] intArray)
    {
        int tempValue;
        int n=intArray.length-1;
        int j=0;
        do {
            for(int i=0; i<intArray.length-1; i++)
            {
                j=i+1;
                if(intArray[i]<intArray[j])
                {
                    tempValue=intArray[i];
                    intArray[i]=intArray[j];
                    intArray[j]=tempValue;
                }
            }
        n--;
        }while(n>=1);
        return intArray;
    }


    public static void main(String[] args) {
        System.out.println("Give me amount of elements");
        Scanner scanner=new Scanner(System.in);
        int amount = scanner.nextInt();
        int[] array=getIntegers(amount);
        int[] sortArray=sortIntegers(array);

        printArray(sortArray);

    }
}
