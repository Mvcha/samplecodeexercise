package com.company;

public class Hamburger {
    private String name;
    private String meat;
    private double price;
    private String breadRollType;

    private String addition1Name;
    private String addition2Name;
    private String addition3Name;
    private String addition4Name;

    private double addition1Price;
    private double addition2Price;
    private double addition3Price;
    private double addition4Price;

    public Hamburger(String name, String meat, double price, String breadRollType) {
        this.name = name;
        this.meat = meat;
        this.price = price;
        this.breadRollType = breadRollType;

        System.out.println(name+" hamburger on "+breadRollType+" with "+meat+" it's a cost "+price);
    }

    public void addHamburgerAddition1(String name, double price)
    {
        System.out.println("Added "+name+" for an extra "+price);
        this.addition1Name=name;
        this.addition1Price=price;
    }
    public void addHamburgerAddition2(String name, double price)
    {
        System.out.println("Added "+name+" for an extra "+price);
        this.addition2Name=name;
        this.addition2Price=price;
    }
    public void addHamburgerAddition3(String name, double price)
    {
        System.out.println("Added "+name+" for an extra "+price);
        this.addition3Name=name;
        this.addition3Price=price;
    }
    public void addHamburgerAddition4(String name, double price)
    {
        System.out.println("Added "+name+" for an extra "+price);
        this.addition4Name=name;
        this.addition4Price=price;
    }
    public double itemizeHamburger()
    {
        double totalHamburgerCost=price;
        if(addition1Name!=null) totalHamburgerCost+=addition1Price;

        if(addition2Name!=null) totalHamburgerCost+=addition2Price;

        if(addition3Name!=null) totalHamburgerCost+=addition3Price;

        if(addition4Name!=null) totalHamburgerCost+=addition4Price;

        return totalHamburgerCost;
    }
}
