public class SharedDigit {
    public static boolean hasSharedDigit(int firstNumber, int secondNumber)
    {
        if(firstNumber<10 || firstNumber>99 ||secondNumber<10 || secondNumber>99)
        {
            return false;
        }
        int lastDigitFirstNumber;
        int lastDigitSecondNumber;
        int newFirstNumber=firstNumber;
        int newSecondNumber;

        for(int i=1; newFirstNumber>=1;i++)
        {
            lastDigitFirstNumber=newFirstNumber%10;
            newFirstNumber/=10;

            newSecondNumber=secondNumber;
            for(int j=1; newSecondNumber>=1;j++)
            {
                lastDigitSecondNumber=newSecondNumber%10;
                newSecondNumber/=10;
               if(lastDigitFirstNumber==lastDigitSecondNumber)
                {
                    return true;
                }
            }

        }
        return false;

    }
}
