package com.company;

public class HealthyBurger extends Hamburger {

    private String healthyExtra1Name;
    private String healthyExtra2Name;
    private double healthyExtra1Price;
    private double healthyExtra2Price;


    public HealthyBurger(String meat, double price) {
        super("Healthy burger", meat, price, "Brown rye roll");

    }

    public void addHealthyAddition1(String name, double price)
    {
        System.out.println("Added "+name+" for an extra "+price);
        this.healthyExtra1Name=name;
        this.healthyExtra1Price=price;
    }
    public void addHealthyAddition2(String name, double price)
    {
        System.out.println("Added "+name+" for an extra "+price);
        this.healthyExtra2Name=name;
        this.healthyExtra2Price=price;
    }

    @Override
    public double itemizeHamburger() {
        double totalCost = super.itemizeHamburger();
        if(healthyExtra1Name!=null) totalCost+=healthyExtra1Price;
        if(healthyExtra2Name!=null) totalCost+=healthyExtra2Price;
        
        return totalCost;
    }
}
