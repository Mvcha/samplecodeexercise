package com.company;

import java.util.ArrayList;

public class Branch {
    private String name;
    private ArrayList<Customer> customers;

    public Branch(String name) {
        this.name = name;
        this.customers = new ArrayList<Customer>();
    }

    public String getName() {
        return name;
    }

    public ArrayList<Customer> getCustomers() {
        return customers;
    }
    public boolean newCustomer(String nameCustomer, double initialTransaction)
    {
        if(findCustomer(nameCustomer)==null)
        {
            System.out.println("Użytkownika jak "+nameCustomer+" nie ma");
            customers.add(new Customer(nameCustomer, initialTransaction));
            return true;
        }
        System.out.println("Istnieje");
        return false;
    }
    public boolean addCustomerTransaction(String nameCustomer, double transaction)
    {
        if(findCustomer(nameCustomer)!=null)
        {
            Customer customer=findCustomer(nameCustomer);
            customer.addTransaction(transaction);
            return true;
        }
        return false;
    }
    private Customer findCustomer(String nameCustomer)
    {
        for(Customer customer : customers)
        {
            if(customer.getName().equals(nameCustomer))
            {
                return customer;
            }
        }
        return null;
    }
}
