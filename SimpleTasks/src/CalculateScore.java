public class CalculateScore {

    public static int calculateScore(String playerName, int score)
    {
        System.out.println("Player: " + playerName + " scored " +score+" points");
        return score*1000;
    }

    public static int calculateScore( int score)
    {
        System.out.println("Unnamed player:  scored " +score+" points");
        return score*1000;
    }

    public static int calculateScore()
    {
        System.out.println("Lack of player and score");
        return 0;
    }

    public static double calcFeetAndInchesToCentimeters(double feet, double inches)
    {
        if(feet>=0 && inches>=0 && inches <=12)
        {
            double centimeters=(inches*2.54)+(feet*30.48);
            System.out.println(centimeters+" centimeters comprise with "+feet+" and "+inches+" inches");
            return centimeters;
        }
        else
            System.out.println("Invalid feet or inches");
            return -1;
    }

    public static double calcFeetAndInchesToCentimeters(double inches)
    {
        if(inches>=0 )
        {
            double feet = Math.round(inches*0.083);
            double remainingInches = inches%12;
            System.out.println(inches+" inches is equal to "+ feet +" feet and "+ remainingInches +" remaining inches");
            return calcFeetAndInchesToCentimeters(feet, remainingInches);
        }
        else
            System.out.println("Invalid inches");
            return -1;
    }

}
