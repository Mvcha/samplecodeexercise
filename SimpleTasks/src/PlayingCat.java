public class PlayingCat {

    public static boolean isCatPlaying(boolean summer, int temperature)
    {
        if(summer==false && temperature>=25 && temperature<36)
            return true;
        else if(summer==true && temperature>=25 && temperature<46)
            return true;
        else
            return false;
    }
}
