public class Main {

    public static void main(String[] args) {
       // SpeedConverter.toMilesPerHour(10.5);
       /* MegaBytesConverter.printMegaBytesAndKiloBytes(2500);
        BarkingDog.shouldWakeUp(true,1);
        BarkingDog.shouldWakeUp(false,2);
        BarkingDog.shouldWakeUp(true,8);
        BarkingDog.shouldWakeUp(true,-1);*/
       // LeapYear.isLeapYear(1600);
       // LeapYear.isLeapYear(2017);
        DecimalComparator.areEqualByThreeDecimalPlaces(-3.1756, -3.175);
        DecimalComparator.areEqualByThreeDecimalPlaces(3.175, 3.176);
        //CalculateScore.calculateScore("John",1000);



            //  int newScore= calculateScore("Marek",500);
            //   System.out.println("New score is: "+ newScore);
            // calculateScore(1000);
            // calculateScore();
        CalculateScore.calcFeetAndInchesToCentimeters(12);
        CalculateScore.calcFeetAndInchesToCentimeters(6,12);
            double centimeters =  CalculateScore.calcFeetAndInchesToCentimeters(6,0);
            System.out.println("------------");
        CalculateScore.calcFeetAndInchesToCentimeters(100);
            System.out.println("------------");
        CalculateScore.calcFeetAndInchesToCentimeters(157);

        System.out.println("Next exercise");
        System.out.println(SecondAndMinutesChallenge.getDurationString(65,45));
        System.out.println(SecondAndMinutesChallenge.getDurationString(3945));

        System.out.println("Next exercise");
        AreaCalculator.area(5.0);
        AreaCalculator.area(-1);
        AreaCalculator.area(5.0,4.0);
        AreaCalculator.area(-1.0,4.0);

        System.out.println("Next exercise");
        MinutesToYearsDaysCalculator.printYearsAndDays(525600);
        MinutesToYearsDaysCalculator.printYearsAndDays(1051200);
        MinutesToYearsDaysCalculator.printYearsAndDays(561600);
        System.out.println("Next exercise");
        IntEqualityPrinter.printEqual(1,1,1);
        IntEqualityPrinter.printEqual(1,1,2);
        IntEqualityPrinter.printEqual(-1,-1,-1);
        System.out.println("Next exercise");
        System.out.println(PlayingCat.isCatPlaying(true,10));
        System.out.println(PlayingCat.isCatPlaying(false,36));
        System.out.println(PlayingCat.isCatPlaying(false,35));
        System.out.println("Next exercise");
        System.out.println("------------");
        Statement.switchValueChallenge('A');
        Statement.switchValueChallenge('a');
        DayOfTheWeekChallenge.printDayOfTheWeek(1);
        DayOfTheWeekChallenge.printDayOfTheWeek(12);
        NumberInWord.printNumberInWord(1);
        NumberInWord.printNumberInWord(5);

        System.out.println("Next exercise NumberOfDaysInMonth");
        System.out.println("------------");
        System.out.println(NumberOfDaysInMonth.getDaysInMonth(1,2020));
        System.out.println(NumberOfDaysInMonth.getDaysInMonth(2,2020));
        System.out.println(NumberOfDaysInMonth.getDaysInMonth(2,2018));
        System.out.println(NumberOfDaysInMonth.getDaysInMonth(-1,2020));
        System.out.println(NumberOfDaysInMonth.getDaysInMonth(1,-2020));
        System.out.println("Next exercise SumOddRange");
        System.out.println("------------");
        System.out.println(SumOddRange.sumOdd(1,100));
        System.out.println(SumOddRange.sumOdd(-1,100));
        System.out.println(SumOddRange.sumOdd(100,100));
        System.out.println(SumOddRange.sumOdd(13,13));
        System.out.println(SumOddRange.sumOdd(100,-100));
        System.out.println(SumOddRange.sumOdd(100,1000));

        System.out.println("Next exercise is Palindrome");


        System.out.println(NumberPalindrome.isPalindrome(-1221));
        System.out.println(NumberPalindrome.isPalindrome(707));
        System.out.println(NumberPalindrome.isPalindrome(11212));
        System.out.println("Next exercise with sum last and first digit");
        System.out.println(FirstLastDigitSum.sumFirstAndLastDigit(257));
        System.out.println(FirstLastDigitSum.sumFirstAndLastDigit(0));
        System.out.println(FirstLastDigitSum.sumFirstAndLastDigit(5));
        System.out.println(FirstLastDigitSum.sumFirstAndLastDigit(-10));

        System.out.println("Next exercise with sum digit sum");
        System.out.println(EvenDigitSum.getEvenDigitSum(123456789));
        System.out.println(EvenDigitSum.getEvenDigitSum(252));
        System.out.println(EvenDigitSum.getEvenDigitSum(-22));
        System.out.println(EvenDigitSum.getEvenDigitSum(2000));

        System.out.println("Check if the same digit in two numbers");
     //   System.out.println(SharedDigit.hasSharedDigit(12,23));
      //  System.out.println(SharedDigit.hasSharedDigit(9,99));
      //  System.out.println(SharedDigit.hasSharedDigit(15,55));
        System.out.println(SharedDigit.hasSharedDigit(12,13));

        System.out.println("Checking last digit in 3 number");
        System.out.println(LastDigitChecker.hasSameLastDigit(41,22,71));
        System.out.println(LastDigitChecker.hasSameLastDigit(23,32,42));
        System.out.println(LastDigitChecker.hasSameLastDigit(9,99,999));

        System.out.println("Find the biggest divide two numbers");
        System.out.println(GreatestCommonDivisor.getGreatestCommonDivisor(25,15));
        System.out.println(GreatestCommonDivisor.getGreatestCommonDivisor(12,30));
        System.out.println(GreatestCommonDivisor.getGreatestCommonDivisor(9,18));
        System.out.println(GreatestCommonDivisor.getGreatestCommonDivisor(81,153));

        System.out.println("next exercise");
        FactorPrinter.printFactors(6);

        System.out.println("next exercise - perfect number");
        System.out.println(PerfectNumber.isPerfectNumber(6));
        System.out.println(PerfectNumber.isPerfectNumber(28));
        System.out.println(PerfectNumber.isPerfectNumber(5));
        System.out.println(PerfectNumber.isPerfectNumber(-1));

        System.out.println("Flour packer");
        System.out.println(FlourPacker.canPack(1,0,4));
        System.out.println(FlourPacker.canPack(1,0,5));
        System.out.println(FlourPacker.canPack(0,5,4));
        System.out.println(FlourPacker.canPack(2,2,11));
        System.out.println(FlourPacker.canPack(-3,2,12));
        System.out.println(FlourPacker.canPack(0,5,6));
        System.out.println(FlourPacker.canPack(2,1,5));

       // System.out.println("Exercise with user input");
       // ReadingUserInputChallenge.readUserInput();
       // MinimumAndMaximumChallenge.minAndMaximumInput();
       // InputCalculator.inputThenPrintSumAndAverage();

        System.out.println("Exercise with painting area");
        System.out.println(PaintJob.getBucketCount(3.4,2.1,1.5,2));
        System.out.println(PaintJob.getBucketCount(2.75,3.25,2.5,1));
        System.out.println(PaintJob.getBucketCount(3.4,2.1,1.5));
        System.out.println(PaintJob.getBucketCount(3.4,1.5));
        System.out.println(PaintJob.getBucketCount(3.4,1.5));

    }
}
