package com.company;

public class Point {

    private int x;
    private int y;

    public Point()
    {
        this.x=0;
        this.y=0;
    }
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double distance()
    {
        int x2=0;
        int y2=0;
        return (double)Math.sqrt((x2-x)*(x2-x)+(y2-y)*(y2-y));
    }

    public double distance(int x2, int y2)
    {
        return (double)Math.sqrt((x2-x)*(x2-x)+(y2-y)*(y2-y));
    }

    public double distance(Point second) {
        return (double)Math.sqrt((second.x-x)*(second.x-x)+(second.y-y)*(second.y-y));
    }
}
