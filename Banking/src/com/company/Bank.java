package com.company;

import java.util.ArrayList;

public class Bank {
    private String name;
    private ArrayList<Branch> branches;

    public Bank(String name) {
        this.name = name;
        this.branches = new ArrayList<Branch>();
    }

    public boolean addBranch(String name)
    {
        if (findBranch(name)!=null)
        {
            System.out.println("Branch already exist");
            return false;
        }
        else
        {
            Branch branch=new Branch(name);
            branches.add(branch);
            return true;
        }

    }

    public boolean addCustomer(String nameBranch, String nameCustomer, double initialTransaction)
    {
        Branch branch=findBranch(nameBranch);
        if(branch!=null)
        {
            System.out.println("Branch istnieje");
            return branch.newCustomer(nameCustomer,initialTransaction);
        }
        return false;
    }

    public boolean addCustomerTransaction(String nameBranch, String nameCustomer, double transaction)
    {
        if(findBranch(nameBranch)!=null)
        {
            Branch branch=findBranch(nameBranch);

            return branch.addCustomerTransaction(nameCustomer,transaction);
        }
        return false;
    }

    private Branch findBranch(String nameBranch)
    {
        for (Branch branch : branches)
        {
           if(branch.getName().equals(nameBranch))
           {
               int parameter= branches.indexOf(branch);
               branch=branches.get(parameter);
               return branch;
           }
        }
        return null;
    }

    public boolean listCustomers(String nameBranch, boolean printTransaction)
    {

        if(findBranch(nameBranch)!=null)
        {
            Branch branch=findBranch(nameBranch);
            if(printTransaction==true)
            {
                System.out.println("Customer details for branch " +branch.getName());
                ArrayList<Customer> branchCustomers = branch.getCustomers();

                int i=1;
                for (Customer customer : branchCustomers)
                {
                    ArrayList<Double> transactions = customer.getTransactions();
                    System.out.println("Customer: "+customer.getName()+"["+i+"]");
                    System.out.println("Transactions");
                    for (int j=0; j<transactions.size();j++)
                    {
                        System.out.println("[" + (j+1) + "]  Amount "  + transactions.get(j));
                    }

                    i++;
                }

            }
            else
            {
                System.out.println("Customer details for branch " +branch.getName());
                ArrayList<Customer> branchCustomers = branch.getCustomers();
                int i=1;
                for (Customer customer : branchCustomers)
                {
                    System.out.println("Customer: "+customer.getName()+"["+i+"]");
                    i++;
                }
                return true;
            }

            return true;
        }
        return false;
    }
}
