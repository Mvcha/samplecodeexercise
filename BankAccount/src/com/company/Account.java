package com.company;

public class Account {


    private  String accountNumber;
    private double balance;
    private String customerName;
    private String customerEmailAddress;
    private String customerPhoneNumber;

public Account()
{
    this("56788", 2.50, "Default name", "Default address", "default phone");
    System.out.println("Empty constructor");
}

    public Account(String accountNumber, double balance,  String customerName, String customerEmailAddress, String customerPhoneNumber) {
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.customerName = customerName;
        this.customerEmailAddress = customerEmailAddress;
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public Account(String customerName, String customerEmailAddress, String customerPhoneNumber) {
        this("9999",100.55, customerName,customerEmailAddress,customerPhoneNumber);
    }


    public void deposit(double depositAmount)
    {
        this.balance+=depositAmount;
        System.out.println("Deposit of "+depositAmount+" made. New balance is "+this.balance);
    }

    public  void withdrawal(double withdrawalAmount)
    {
        if(balance - withdrawalAmount<=0)
        {
            System.out.println("Only "+this.balance + "available. Withdrawal not processed");
        }
        else
        {
            this.balance-=withdrawalAmount;
            System.out.println("Withdrawal of "+ withdrawalAmount+" processed. Remaining balance= "+balance);
        }
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }


    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmailAddress() {
        return customerEmailAddress;
    }

    public void setCustomerEmailAddress(String customerEmailAddress) {
        this.customerEmailAddress = customerEmailAddress;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }
}
