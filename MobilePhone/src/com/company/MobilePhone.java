package com.company;

import java.util.ArrayList;

public class MobilePhone {
    private String myNumber;
    private ArrayList<Contact> myContacts;

    public MobilePhone(String myNumber) {
        this.myNumber = myNumber;
        this.myContacts =new ArrayList<Contact>();
    }

    public boolean addNewContact(Contact contact)
    {
        if(findContact(contact.getName())==-1)
        {
            myContacts.add(contact);
            return true;
        }
        System.out.println("failed");
        return false;

    }

    public boolean updateContact(Contact contactOld, Contact contactNew)
    {
        int index=findContact(contactOld);
        if(index>=0)
        {
            myContacts.set(index,contactNew);
            System.out.println("Contact changed successfully");
            return true;
        }
        System.out.println("Contact has not been changed ");
            return false;
    }
    public void printContacts()
    {
        int j=1;
        System.out.println("Contact List:");
        for (Contact i : myContacts)
        {
            System.out.println(j+".\t"+i.getName()+" -> "+i.getPhoneNumber());
            j++;
        }
    }
    private int findContact(Contact contact)
    {
        int parameter = myContacts.indexOf(contact);
        if(parameter>=0){
            return  parameter;
        }
        return -1;
    }
    private int findContact(String name)
    {
        for(Contact i : myContacts)
        {
            if(i.getName().equals(name))
            {
                int parameter=myContacts.indexOf(i);
                return parameter;
            }
        }
        return -1;
    }

    public Contact queryContact(String name)
    {
        if(findContact(name)>=0){
            Contact contact=myContacts.get(findContact(name));
            return contact;
        }
        return null;
    }

    public boolean removeContact(Contact contact)
    {
        if(findContact(contact)>=0)
        {
            myContacts.remove(findContact(contact));
            return true;
        }
        return false;
    }

}
