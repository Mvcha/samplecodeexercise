package com.company;

public class Main {

    public static void main(String[] args) {

        Person person=new Person();
        person.setFirstName("John");
        person.setLastName("Terr");
        person.setAge(13);

        System.out.println("Full name: "+ person.getFullName());
        System.out.println("Is teen? "+person.isTeen());
    }
}
