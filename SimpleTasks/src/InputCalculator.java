import java.util.Scanner;

public class InputCalculator {

    public static void inputThenPrintSumAndAverage()
    {
        int sum=0;
        long average;
        int i =0;
        Scanner scanner=new Scanner(System.in);
        while (true)
        {
            boolean isInt = scanner.hasNextInt();
            if(isInt)
            {
                int number=scanner.nextInt();
                sum+=number;
                i++;

            }
            else
            {
                average=Math.round((double)sum/i);
                System.out.println("SUM = "+sum+" AVG = "+average);
                break;
            }
            scanner.nextLine();
        }

    }
}
