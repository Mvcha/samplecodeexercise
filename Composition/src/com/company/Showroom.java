package com.company;

public class Showroom {
    private String name;
    private Wall wall1;
    private Wall wall2;
    private Wall wall3;
    private Wall wall4;
    private Tv tv;
    private Ceiling ceiling;
    private Sofa sofa;
    private Chandelier chandelier;

    public Showroom(String name, Wall wall1, Wall wall2, Wall wall3, Wall wall4, Tv tv, Ceiling ceiling, Sofa sofa, Chandelier chandelier) {
        this.name = name;
        this.wall1 = wall1;
        this.wall2 = wall2;
        this.wall3 = wall3;
        this.wall4 = wall4;
        this.tv = tv;
        this.ceiling = ceiling;
        this.sofa = sofa;
        this.chandelier = chandelier;
    }

    public Chandelier getChandelier() {
        return chandelier;
    }

    public void coverSofa(){
        System.out.println("Covering sofa");
        sofa.cover();
    }
}
