package com.company;

public class Main {

    public static void main(String[] args) {
        Dimensions dimensions=new Dimensions(20,20,5);
	    Case theCase= new Case("220B", "Dell", "240W", dimensions);

        Monitor theMonitor= new Monitor("25inch ", "Asus", 26, new Resolution(2540,1440));

        Motherboard theMotherboard= new Motherboard("XX-200", "Acer", 4, 6,"1.213");

        PC thePC = new PC(theCase,theMonitor,theMotherboard);
       // thePC.getMonitor().drawPixelAt(1500,1200,"red");
       // thePC.getMotherboard().loadProgram("Windows XP");
       // thePC.getTheCase().pressPowerButton();
        thePC.powerUP();

        Wall wall1= new Wall ("West");
        Wall wall2= new Wall ("East");
        Wall wall3= new Wall ("South");
        Wall wall4= new Wall ("North");
        Ceiling ceiling=new Ceiling(10,"white");
        Tv tv= new Tv("Samsung", "SAxz-52", 52);
        Sofa sofa=new Sofa(4, "Ikea");
        Chandelier chandelier=new Chandelier(4, "Ikea");
        Showroom showroom=new Showroom("Salon", wall1, wall2,wall3,wall4, tv, ceiling,sofa, chandelier);
        showroom.coverSofa();
        showroom.getChandelier().turnOn();
    }

}
