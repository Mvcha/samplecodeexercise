package com.company;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class Album {
    private String name;
    private String artist;
    private ArrayList<Song> songs;

    public Album(String name, String artist) {
        this.name = name;
        this.artist = artist;
        this.songs = new ArrayList<Song>();
    }
    public boolean addSong(String title, double duration)
    {
        if(findSong(title)==null)
        {
            Song song=new Song(title,duration);
            songs.add(song);
            return true;
        }
        return false;
    }
    private Song findSong(String title)
    {
        for(int i=0; i<songs.size();i++)
        {
            Song song=songs.get(i);
            if(song.getTitle().equals(title))
            {
                return song;
            }
        }
        return null;
    }
    public boolean addToPlayList(String title, LinkedList<Song> playlist)
    {
        ListIterator<Song> songListIterator =playlist.listIterator();

        if(findSong(title)!=null)
        {
            songListIterator.add(findSong(title));
            return true;
        }
        else
        {
            System.out.println(title +" doesn't exist in album");
            return false;
        }

    }
    public boolean addToPlayList(int trackNumber, LinkedList<Song> playlist)
    {
        int index = trackNumber-1;
        if(index>=0 && (index<=songs.size()))
        {
            playlist.add(songs.get(index));
            return true;
        }
        System.out.println("This album does not have a track "+trackNumber);
        return false;
    }
}
