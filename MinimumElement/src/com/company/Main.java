package com.company;

import java.util.Scanner;

public class Main {

    private static int readInteger()
    {
        Scanner scanner=new Scanner(System.in);
        int amountElements=scanner.nextInt();
        return amountElements;
    }

    private static int[] readElements(int amountElements)
    {
        int[] intArray=new int[amountElements];

        Scanner scanner=new Scanner(System.in);
        for(int i=0; i<intArray.length;i++)
        {
            System.out.println("Give "+(i+1)+" digit ");
            intArray[i]=scanner.nextInt();
        }
        return intArray;
    }

    private static int findMin(int[] intArray)
    {
        int minValue=intArray[0];
        for(int i : intArray)
        {
            if(i<minValue)minValue=i;
        }
        return minValue;
    }

    public static void main(String[] args) {

        Scanner scanner=new Scanner(System.in);
        System.out.println("How many digits do you want to enter?");
        int[] intArray=readElements(readInteger());
        System.out.println("Minimum element in array is: "+findMin(intArray));
    }
}
