import java.util.Scanner;

public class ReadingUserInputChallenge {

    public static void readUserInput()
    {
        Scanner scanner=new Scanner(System.in);

        int sum=0;
        for(int i=0; i<10; i++)
        {
            System.out.println("Enter number #"+(i+1));
            boolean isInt=scanner.hasNextInt();

            if(isInt)
            {
                int number=scanner.nextInt();
                sum+=number;
            }
            else
            {
                System.out.println("Invalid number");
                i--;
            }
            scanner.nextLine();
        }

        System.out.println("Sum is "+sum);
        scanner.close();
    }




}
